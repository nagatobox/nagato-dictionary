

SHOW_VERSION = {
    "action": "store_true",
    "dest": "show_version",
    "default": False,
    "help": "show version"
}

QUERY = {
    "metavar": "query",
    "type": str,
    "nargs": "*",
    "help": "word to search"
}
