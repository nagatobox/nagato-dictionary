
TEMPLATE = "TEMPLATE"

MESSAGE_QUIT = \
    "<span size='large'><u>WARNING !!</u></span>\n"\
    "\n"\
    "Do you really want to close nagato-dictionary ?\n"\
    "\n"

BUTTONS_CLOSE = BUTTONS = ["Cancel", "Close"]

QUIT = {"message": MESSAGE_QUIT, "buttons": BUTTONS_CLOSE}
