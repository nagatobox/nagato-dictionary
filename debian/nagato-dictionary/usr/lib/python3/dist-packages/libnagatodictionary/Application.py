
from libnagato4.Object import NagatoObject
from libnagatodictionary.dictionary.Dictionary import NagatoDictionary


class NagatoApplication(NagatoObject):

    def run(self):
        yuki_queries = self._enquiry("YUKI.N > args", "query")
        if len(yuki_queries) == 0:
            print("YUKI.N > no queries")
            return
        else:
            NagatoDictionary(self)

    def __init__(self, parent):
        self._parent = parent
