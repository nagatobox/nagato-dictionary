
import sqlite3
from libnagato4.Object import NagatoObject

PATH = "/usr/share/com.gitlab.nagatobox.nagato-dictionary/edict.sqlite3"


class NagatoDictionary(NagatoObject):

    def _get_command(self, query):
        try:
            query.encode('ascii')
        except UnicodeEncodeError:
            return "SELECT * FROM GeneralDict WHERE japanese LIKE ?"
        else:
            return "SELECT * FROM GeneralDict WHERE english LIKE ?"

    def _search(self):
        yuki_query = self._enquiry("YUKI.N > args", "query")[0]
        yuki_command = self._get_command(yuki_query)
        yuki_connection = sqlite3.connect(PATH)
        yuki_cursor = yuki_connection.cursor()
        yuki_cursor.execute(yuki_command, ("%"+yuki_query+"%",))
        for yuki_result in yuki_cursor.fetchall():
            if yuki_result[3] is None:
                continue
            print(yuki_result)

    def __init__(self, parent):
        self._parent = parent
        self._search()
